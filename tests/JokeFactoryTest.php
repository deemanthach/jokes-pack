<?php

namespace Deemanthach\Jokespack\Tests;

use Deemanthach\Jokespack\JokesFactory;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class JokeFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_a_random_joke()
    {
        // Create a mock and queue two responses.
        $mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], '{ "type": "success", "value": { "id": 288, "joke": "To be or not to be? That is the question. The answer? Chuck Norris.", "categories": [] } }'),
        ]);

        $handlerStack = HandlerStack::create($mock);

        $client = new Client(['handler'=>$handlerStack]);

        $jokes = new JokesFactory($client);

        $joke = $jokes->getRandomJoke();

        $this->assertSame('To be or not to be? That is the question. The answer? Chuck Norris.', $joke);
    }
}
