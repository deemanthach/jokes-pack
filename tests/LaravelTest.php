<?php

namespace Deemanthach\Jokespack\Tests;

use Deemanthach\Jokespack\Facades\Jokes;
use Deemanthach\Jokespack\JokesServiceProvider;
use Illuminate\Support\Facades\Artisan;
use Orchestra\Testbench\TestCase;

class LaravelTest extends TestCase
{
    protected function getEnvironmentSetUp($app)
    {
        include_once __DIR__.'/../database/migrations/create_jokes_table.php.stub';

        (new \CreateJokesTable())->up();
    }

    protected function getPackageProviders($app)
    {
        return [JokesServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Jokes' => Jokes::class,
        ];
    }

    /** @test */
    public function command_gives_random_joke()
    {
        $this->withoutMockingConsoleOutput();

        Jokes::shouldReceive('getRandomJoke')->once()->andReturn('some joke');

        $this->artisan('cms-jokes');

        $output = Artisan::output();

        $this->assertSame('some joke'.PHP_EOL, $output);
    }

    /** @test */
    public function the_route_can_be_accessed()
    {
        Jokes::shouldReceive('getRandomJoke')->once()->andReturn('some joke');

        $this->get('/rand-joke')
            ->assertViewIs('jokes::index')
            ->assertViewHas('joke')
            ->assertStatus(200);
    }

    /** @test */
    public function it_has_access_to_database()
    {
        $joke = new \Deemanthach\Jokespack\Models\Jokes();
        $joke->joke = 'FUNNY';
        $joke->save();

        $newJoke = \Deemanthach\Jokespack\Models\Jokes::find($joke->id);

        $this->assertSame($newJoke->joke, 'FUNNY');
    }
}
