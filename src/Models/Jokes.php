<?php

namespace Deemanthach\Jokespack\Models;

use Illuminate\Database\Eloquent\Model;

class Jokes extends Model
{
    protected $table = 'jokes';

    protected $guarded = [];
}
