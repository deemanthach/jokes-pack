<?php

namespace Deemanthach\Jokespack\Console;

use Deemanthach\Jokespack\Facades\Jokes;
use Illuminate\Console\Command;

class JokeCommand extends Command
{
    protected $signature = 'cms-jokes';

    protected $description = 'Output a joke from API';

    public function handle()
    {
        $this->info(Jokes::getRandomJoke());
    }
}
