<?php

namespace Deemanthach\Jokespack\Http\Controllers;

use Deemanthach\Jokespack\Facades\Jokes;

class JokesController
{
    public function __invoke()
    {
        return view('jokes::index', [
            'joke' => Jokes::getRandomJoke(),
        ]);
    }
}
