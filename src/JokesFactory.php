<?php

namespace Deemanthach\Jokespack;

use GuzzleHttp\Client;

class JokesFactory
{
    /**
     * @var Client
     */
    private $client;
    const API_ENDPOINT = 'http://api.icndb.com/jokes/random';

    public function __construct(Client $client = null)
    {
        $this->client = $client ?: new Client();
    }

    public function getRandomJoke()
    {
        $response = $this->client->get(self::API_ENDPOINT);

        $joke = json_decode($response->getBody()->getContents());

        return $joke->value->joke;
    }
}
