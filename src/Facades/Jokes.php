<?php

namespace Deemanthach\Jokespack\Facades;

use Illuminate\Support\Facades\Facade;

class Jokes extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'jokes';
    }
}
