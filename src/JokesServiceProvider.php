<?php

namespace Deemanthach\Jokespack;

use Deemanthach\Jokespack\Console\JokeCommand;
use Deemanthach\Jokespack\Http\Controllers\JokesController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class JokesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                JokeCommand::class,
            ]);
        }

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'jokes');

        $this->publishes([
            __DIR__.'/../config/jokes.php' => base_path('config/jokes.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/jokes'),
        ], 'views');

        if (!class_exists('CreateJokesTable')) {
            $this->publishes([
                __DIR__.'/../database/migrations/create_jokes_table.php.stub' => database_path('migrations/'.date('Y_m_d_His', time()).'_create_jokes_table.php'),
            ], 'migrations');
        }

        Route::get(config('jokes.endpoint'), JokesController::class);
    }

    public function register()
    {
        $this->app->bind('jokes', function () {
            return new JokesFactory();
        });

        $this->mergeConfigFrom(__DIR__.'/../config/jokes.php', 'jokes');
    }
}
